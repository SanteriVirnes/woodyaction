﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour
{

    public Transform PlayerSpawnPoint;
    private GameObject PlayerClone;

    void OnMouseDown()
    {
        Application.LoadLevel("levelCottage");
        PlayerClone = GameObject.Find("Peasant 1(Clone)");
        PlayerClone.transform.position = PlayerSpawnPoint.transform.position;
    }
}
