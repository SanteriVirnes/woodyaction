﻿using UnityEngine;
using System.Collections;

public class BatKillScript : MonoBehaviour
{

    public float batWaveInterval;
    public float batSpawningInterval;
    public float batAttackHoldTime;

    public float firstAngleInRadians = 1.0f;
    public int startWaveNumberOfBats = 3;
    public int numberOfWaves = 3;

    public GameObject batPrefab;
    public float spawnRadius = 5.0f;
    public Transform currentBatSpawn;

    public bool killingEnabled = false;
    // public ParticleSystem deadParticle;
    public bool playerAlive = true;

    int numOfClicks = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] Selection = GameObject.FindGameObjectsWithTag("Selection");
        if (killingEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                numOfClicks += 1;
            }
        }

        if (numOfClicks > 0 && numOfClicks < Selection.Length)
        {
            for (int i = 0; i < numOfClicks; i++)
            {
                Selection[i].GetComponent<MeshRenderer>().enabled = true;
            }
        }
        if (numOfClicks == Selection.Length)
        {
            GameObject[] allBats = GameObject.FindGameObjectsWithTag("Bat");
            foreach (var bat in allBats)
            {
                //   Instantiate(deadParticle, bat.transform.position, Quaternion.identity);
                Destroy(bat.gameObject);
                numOfClicks = 0;
            }
        }
        if (numOfClicks > Selection.Length)
        {
            numOfClicks = 0;
        }
        if (!playerAlive)
        {
            Application.LoadLevel("DeathScene");
        }

    }
}
